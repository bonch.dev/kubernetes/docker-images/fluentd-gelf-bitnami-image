FROM bitnami/fluentd:1.16.3

## Install custom Fluentd plugins
USER root
RUN fluent-gem install \
	gelf \
	fluent-plugin-gelf-hs \
	fluent-plugin-gelf
